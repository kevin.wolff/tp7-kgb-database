<?php 

    /*final*/ class Specialite {
        private int $code;
        private string $specialite;

        public function getCode() : int
        {
                return $this->code;
        }

        public function setCode(int $code)
        {
                $this->code = $code;

                return $this;
        }

        public function getSpecialite() : string
        {
                return $this->specialite;
        }

        public function setSpecialite(string $specialite)
        {
                $this->specialite = $specialite;

                return $this;
        }
    }