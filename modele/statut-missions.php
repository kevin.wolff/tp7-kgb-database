<?php 

    /*final*/ class Statutmission {
        private int $codestatut;
        private string $statutmission;
        
        public function getCodestatut() : int
        {
                return $this->codestatut;
        }

        public function setCodestatut(int $codestatut)
        {
                $this->codestatut = $codestatut;

                return $this;
        }

        public function getStatutmission() : string
        {
                return $this->statutmission;
        }

        public function setStatutmission(string $statutmission)
        {
                $this->statutmission = $statutmission;

                return $this;
        }
    }