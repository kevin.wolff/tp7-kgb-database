<?php 

require_once __DIR__.'/humains.php';

    /*final*/ class Contact extends Humain {
        private int $code;
        private string $nomcode;
        private int $codepays;
        
        public function getCode() : int
        {
                return $this->code;
        }

        public function setCode(int $code)
        {
                $this->code = $code;

                return $this;
        }

        public function getNomcode() : string
        {
                return $this->nomcode;
        }

        public function setNomcode(string $nomcode)
        {
                $this->nomcode = $nomcode;
                return $this;
        }

        public function getCodepays() : int
        {
                return $this->codepays;
        }

        public function setCodepays(int $codepays)
        {
                $this->codepays = $codepays;

                return $this;
        }

        // GET & SET ONLY USE FOR DISPLAY VALUE FROM JOIN TABLE

        public function getNamepays() : string
        {
                return $this->namepays;
        }

        public function setNamepays(string $namepays)
        {
                $this->namepays = $namepays;

                return $this;
        }
    }