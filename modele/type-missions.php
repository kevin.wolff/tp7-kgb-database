<?php 

    /*final*/ class Typemission {
        private int $codetypemission;
        private string $typemission;
        
        public function getCodetypemission() : int
        {
                return $this->codetypemission;
        }

        public function setCodetypemission(int $codetypemission)
        {
                $this->codetypemission = $codetypemission;

                return $this;
        }

        public function getTypemission() : string
        {
                return $this->typemission;
        }

        public function setTypemission(string $typemission)
        {
                $this->typemission = $typemission;

                return $this;
        }
    }