<?php 

    /*final*/ class Pays {
        private int $codepays;
        private string $pays;

        public function getCodepays() : int
        {
                return $this->codepays;
        }

        public function setCodepays(int $codepays)
        {
                $this->codepays = $codepays;

                return $this;
        }

        public function getPays() : string
        {
                return $this->pays;
        }

        public function setPays(string $pays)
        {
                $this->pays = $pays;

                return $this;
        }
    }