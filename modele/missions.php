<?php 

    /*final*/ class Mission {
        private int $code;
        private string $nom;
        private string $description;
        private string $nomcode;
        // private int $nbragent;
        // private int $nbrcontact;
        // private int $nbrcible;
        // private int $nbrplanque;
        private int $pays;
        private int $typemission;
        private int $statutmission;
        private int $specialite;
        private string $datedebut;
        private string $datefin;

        public function getCode() : int
        {
                return $this->code;
        }

        public function setCode(int $code)
        {
                $this->code = $code;

                return $this;
        }

        public function getNom() : string
        {
                return $this->nom;
        }

        public function setNom(string $nom)
        {
                $this->nom = $nom;

                return $this;
        }

        public function getDescription() : string
        {
                return $this->description;
        }

        public function setDescription(string $description)
        {
                $this->description = $description;

                return $this;
        }

        public function getNomcode() : string
        {
                return $this->nomcode;
        }

        public function setNomcode(string $nomcode)
        {
                $this->nomcode = $nomcode;

                return $this;
        }

        public function getPays() : int
        {
                return $this->pays;
        }

        public function setPays(int $pays)
        {
                $this->pays = $pays;

                return $this;
        }

        // public function getNbragent() : int
        // {
        //         return $this->nbragent;
        // }

        // public function setNbragent(int $nbragent)
        // {
        //         $this->nbragent = $nbragent;

        //         return $this;
        // }

        // public function getNbrcontact() : int
        // {
        //         return $this->nbrcontact;
        // }

        // public function setNbrcontact(int $nbrcontact)
        // {
        //         $this->nbrcontact = $nbrcontact;

        //         return $this;
        // }

        // public function getNbrcible() : int
        // {
        //         return $this->nbrcible;
        // }

        // public function setNbrcible(int $nbrcible)
        // {
        //         $this->nbrcible = $nbrcible;

        //         return $this;
        // }

        // public function getNbrplanque() : int
        // {
        //         return $this->nbrplanque;
        // }

        // public function setNbrplanque(int $nbrplanque)
        // {
        //         $this->nbrplanque = $nbrplanque;

        //         return $this;
        // }

        public function getTypemission() : int
        {
                return $this->typemission;
        }

        public function setTypemission(int $typemission)
        {
                $this->typemission = $typemission;

                return $this;
        }

        public function getStatutmission() : int
        {
                return $this->statutmission;
        }

        public function setStatutmission(int $statutmission)
        {
                $this->statutmission = $statutmission;

                return $this;
        }

        public function getSpecialite() : int
        {
                return $this->specialite;
        }

        public function setSpecialite(int $specialite)
        {
                $this->specialite = $specialite;

                return $this;
        }

        public function getDatedebut() : string
        {
                return $this->datedebut;
        }

        public function setDatedebut(string $datedebut)
        {
                $this->datedebut = $datedebut;

                return $this;
        }

        public function getDatefin() : string
        {
                return $this->datefin;
        }

        public function setDatefin(string $datefin)
        {
                $this->datefin = $datefin;

                return $this;
        }

         // GET & SET ONLY USE FOR DISPLAY VALUE FROM JOIN TABLE

         public function getNamePays() : string
         {
                 return $this->namepays;
         }
 
         public function setNamePays(string $namepays)
         {
                 $this->namepays = $namepays;
 
                 return $this;
         }
 
         public function getNameSpe() : string
         {
                 return $this->namespe;
         }
 
         public function setNameSpe(string $namespe)
         {
                 $this->namespe = $namespe;
 
                 return $this;
         }
 
         public function getNameType() : string
         {
                 return $this->nametype;
         }
 
         public function setNameType(string $nametype)
         {
                 $this->nametype = $nametype;
 
                 return $this;
         }
 
         public function getNameStatut() : string
         {
                 return $this->namestatut;
         }
 
         public function setNameStatut(string $namestatut)
         {
                 $this->namestatut = $namestatut;
 
                 return $this;
         }
    }