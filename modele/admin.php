<?php 

    /*final*/ class Admin {
        private int $codeadmin;
        private string $nom;
        private string $prenom;
        private string $email;
        private string $mdp;
        private string $datecreation;

        public function getCodeadmin() : int
        {
                return $this->codeadmin;
        }

        public function setCodeadmin(int $codeadmin)
        {
                $this->codeadmin = $codeadmin;

                return $this;
        }

        public function getNom() : string
        {
                return $this->nom;
        }

        public function setNom(string $nom)
        {
                $this->nom = $nom;

                return $this;
        }

        public function getPrenom() : string
        {
                return $this->prenom;
        }

        public function setPrenom(string $prenom)
        {
                $this->prenom = $prenom;

                return $this;
        }

        public function getEmail() : string
        {
                return $this->email;
        }

        public function setEmail(string $email)
        {
                $this->email = $email;

                return $this;
        }

        public function getMdp() : string
        {
                return $this->mdp;
        }

        public function setMdp(string $mdp)
        {
                $this->mdp = $mdp;

                return $this;
        }

        public function getDatecreation() : string
        {
                return $this->datecreation;
        }

        public function setDatecreation(string $datecreation)
        {
                $this->datecreation = $datecreation;

                return $this;
        }
    }