<?php 

    /*final*/ class Agentspe {
        private int $codeagent;
        private int $codespe;

        public function getCodeagent() : int
        {
                return $this->codeagent;
        }

        public function setCodeagent(int $codeagent)
        {
                $this->codeagent = $codeagent;

                return $this;
        }

        public function getCodespe() : int
        {
                return $this->codespe;
        }

        public function setCodespe(int $codespe)
        {
                $this->codespe = $codespe;

                return $this;
        }
    }