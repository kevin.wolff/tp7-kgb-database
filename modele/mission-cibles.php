<?php 

    /*final*/ class Missioncible {
        private int $codemission;
        private int $codecible;

        public function getCodemission() : int
        {
                return $this->codemission;
        }

        public function setCodemission(int $codemission)
        {
                $this->codemission = $codemission;

                return $this;
        }

        public function getCodecible() : int
        {
                return $this->codecible;
        }

        public function setCodecible(int $codecible)
        {
                $this->codecible = $codecible;

                return $this;
        }
    }