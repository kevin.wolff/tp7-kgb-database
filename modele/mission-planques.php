<?php 

    /*final*/ class Missionplanque {
        private int $codemission;
        private int $codeplanque;

        public function getCodemission() : int
        {
                return $this->codemission;
        }

        public function setCodemission(int $codemission)
        {
                $this->codemission = $codemission;

                return $this;
        }

        public function getCodeplanque() : int
        {
                return $this->codeplanque;
        }

        public function setCodeplanque(int $codeplanque)
        {
                $this->codeplanque = $codeplanque;

                return $this;
        }
    }