<?php 

    /*final*/ class Planque {
        private int $code;
        private string $nomcode;
        private string $adresse;
        private int $pays;
        private int $codetype;

        public function getCode() : int
        {
                return $this->code;
        }

        public function setCode(int $code)
        {
                $this->code = $code;

                return $this;
        }

        public function getNomcode() : string
        {
                return $this->nomcode;
        }

        public function setNomcode(string $nomcode)
        {
                $this->nomcode = $nomcode;

                return $this;
        }

        public function getAdresse() : string
        {
                return $this->adresse;
        }

        public function setAdresse(string $adresse)
        {
                $this->adresse = $adresse;

                return $this;
        }

        public function getPays() : int
        {
                return $this->pays;
        }

        public function setPays(int $pays)
        {
                $this->pays = $pays;

                return $this;
        }

        public function getCodetype() : int
        {
                return $this->codetype;
        }

        public function setCodetype(int $codetype)
        {
                $this->codetype = $codetype;

                return $this;
        }

        // GET & SET ONLY USE FOR DISPLAY VALUE FROM JOIN TABLE

        public function getNamePays() : string
        {
                return $this->namepays;
        }

        public function setNamePays(string $namepays)
        {
                $this->namepays = $namepays;

                return $this;
        }

        public function getNameType() : string
        {
                return $this->nametype;
        }

        public function setNameType(string $nametype)
        {
                $this->nametype = $nametype;

                return $this;
        }
    }