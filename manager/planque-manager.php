<!-- MANAGER PLANQUE -->

<?php

require dirname(__DIR__).'/autoloader.php';
require dirname(__DIR__).'/modele/planques.php';

// CLASS PLANQUE
class PlanqueManager extends DBManager{

    public function getAll() {
        $result = [];

        $stmt = $this->getConnexion()->query('SELECT * FROM planque JOIN pays ON planque.code_pays = pays.code_pays
                                                                    JOIN type_de_planque ON planque.code_type = type_de_planque.code_planque');

        while($row = $stmt->fetch()) {
            $planque = new Planque();
            $planque->setCode($row['code_planque']);
            $planque->setNomcode($row['nom_code']);
            $planque->setAdresse($row['adresse_planque']);
            $planque->setPays($row['code_pays']);
            $planque->setCodetype($row['code_type']);
            // INFO FROM JOIN TABLE : PAYS
            $planque->setNamePays($row['pays']);
            $planque->setNameType($row['type_planque']);

            $result[] = $planque;
        }

        return $result;
    }

    public function add($planque) {
        $stmt = $this->getConnexion()->prepare('INSERT INTO planque VALUES 
                                                (:code, 
                                                :nomcode,
                                                :adresse, 
                                                :codepays, 
                                                :codetype);');
                                                
        $stmt->execute(['code' => NULL,
                        'nomcode' => $planque->getNomcode(),
                        'adresse' => $planque->getAdresse(),
                        'codepays' => $planque->getPays(),
                        'codetype' => $planque->getCodetype()]);
        return true;
    }

    public function  delete($planque) {

        $stmt = $this->getConnexion()->prepare('DELETE FROM planque WHERE code_planque = :code');

        $result = $stmt->execute(['code' => $planque->getCode()]);

        return $result;
    }
}