<!-- MANAGER TYPE MISSION -->

<?php

require dirname(__DIR__).'/autoloader.php';
require dirname(__DIR__).'/modele/type-missions.php';

// CLASS TYPE MISSION
class TypeMissionManager extends DBManager{

    public function getAll() {
        $result = [];

        $stmt = $this->getConnexion()->query('SELECT * FROM type_mission');

        while($row = $stmt->fetch()) {
            $typemission = new Typemission();
            $typemission->setCodetypemission($row['code_type_mission']);
            $typemission->setTypemission($row['type_mission']);

            $result[] = $typemission;
        }

        return $result;
    }

    public function add($typemission) {
        $stmt = $this->getConnexion()->prepare('INSERT INTO type_mission VALUES 
                                                (:code, 
                                                :typem);');
                                                
        $stmt->execute(['code' => NULL,
                        'typem' => $typemission->getTypemission()]);
        return true;
    }

    public function  delete($type) {

        $stmt = $this->getConnexion()->prepare('DELETE FROM type_mission WHERE code_type_mission = :code');

        $result = $stmt->execute(['code' => $type->getCodetypemission()]);

        return $result;
    }
}