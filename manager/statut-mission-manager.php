<!-- MANAGER STATUT MISSION -->

<?php

require dirname(__DIR__).'/autoloader.php';
require dirname(__DIR__).'/modele/statut-missions.php';

// CLASS STATUT MISSION
class StatutMissionManager extends DBManager{

    public function getAll() {
        $result = [];

        $stmt = $this->getConnexion()->query('SELECT * FROM statut_mission');

        while($row = $stmt->fetch()) {
            $statutmission = new Statutmission();
            $statutmission->setCodestatut($row['code_statut']);
            $statutmission->setStatutmission($row['statut_de_mission']);

            $result[] = $statutmission;
        }

        return $result;
    }

    public function add($statutmission) {
        $stmt = $this->getConnexion()->prepare('INSERT INTO statut_mission VALUES 
                                                (:code, 
                                                :statutm);');
                                                
        $stmt->execute(['code' => NULL,
                        'statutm' => $statutmission->getStatutmission()]);
        return true;
    }

    public function  delete($statut) {

        $stmt = $this->getConnexion()->prepare('DELETE FROM statut_mission WHERE code_statut = :code');

        $result = $stmt->execute(['code' => $statut->getCodestatut()]);

        return $result;
    }
}