<!-- MANAGER AGENT -->

<?php

require dirname(__DIR__).'/autoloader.php';
require dirname(__DIR__).'/modele/agents.php';

// CLASS AGENT
class AgentManager extends DBManager{

    public function getAll() {
        $result = [];

        $stmt = $this->getConnexion()->query('SELECT * FROM agent JOIN pays ON agent.code_pays = pays.code_pays 
                                                                  JOIN specialite ON agent.code_spe = specialite.code_spe');

        while($row = $stmt->fetch()) {
            $agent = new Agent();
            $agent->setCode($row['code_agent']);
            $agent->setNomcode($row['nom_code']);
            $agent->setNom($row['nom_agent']);
            $agent->setPrenom($row['prenom_agent']);
            $agent->setDatenaissance($row['date_naissance_agent']);
            $agent->setCodespe($row['code_spe']);
            $agent->setCodepays($row['code_pays']);
            // INFO FROM JOIN TABLE : PAYS, SPECIALITE
            $agent->setNamePays($row['pays']);
            $agent->setNameSpe($row['specialite']);

            $result[] = $agent;
        }

        return $result;
    }

    public function add($agent) {
        $stmt = $this->getConnexion()->prepare('INSERT INTO agent VALUES 
                                                (:code, 
                                                :nomcode,
                                                :nom, 
                                                :prenom, 
                                                :datena, 
                                                :codespe, 
                                                :codepays);');

        $stmt->execute(['code' => NULL,
                        'nomcode' => $agent->getNomcode(),
                        'nom' => $agent->getNom(),
                        'prenom' => $agent->getPrenom(),
                        'datena' => $agent->getDatenaissance(),
                        'codespe' => $agent->getCodespe(),
                        'codepays' => $agent->getCodepays()]);
        return true;
    }

    public function  delete($agent) {

        $stmt = $this->getConnexion()->prepare('DELETE FROM agent WHERE code_agent = :code');

        $result = $stmt->execute(['code' => $agent->getCode()]);

        return $result;
    }
}