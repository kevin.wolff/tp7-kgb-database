<!-- MANAGER MISSION -->

<?php

require dirname(__DIR__).'/autoloader.php';
require dirname(__DIR__).'/modele/missions.php';

// CLASS MISSION
class MissionManager extends DBManager{

    public function getAll() {
        $result = [];

        $stmt = $this->getConnexion()->query('SELECT * FROM mission JOIN type_mission ON mission.code_type_mission = type_mission.code_type_mission 
                                                                    JOIN specialite ON mission.code_spe = specialite.code_spe 
                                                                    JOIN statut_mission ON mission.code_statut = statut_mission.code_statut 
                                                                    JOIN pays ON mission.code_pays = pays.code_pays');

        while($row = $stmt->fetch()) {
            $mission = new Mission();
            $mission->setCode($row['code_mission']);
            $mission->setNom($row['nom_mission']);
            $mission->setDescription($row['description_mission']);
            $mission->setNomcode($row['nom_code_mission']);
            $mission->setStatutmission($row['code_statut']);
            $mission->setSpecialite($row['code_spe']);
            $mission->setPays($row['code_pays']);
            $mission->setTypemission($row['code_type_mission']);
            // $mission->setNbragent($row['nbr_agent']);
            // $mission->setNbrcontact($row['nbr_contact']);
            // $mission->setNbrcible($row['nbr_cible']);
            // $mission->setNbrplanque($row['nbr_planque']);
            $mission->setDatedebut($row['date_debut']);
            $mission->setDatefin($row['date_fin']);
            // INFO FROM JOIN TABLE : PAYS, SPECIALITE, TYPE_MISSION, STATUT_MISSION 
            $mission->setNamePays($row['pays']);
            $mission->setNameSpe($row['specialite']);
            $mission->setNameType($row['type_mission']);
            $mission->setNameStatut($row['statut_de_mission']);

            $result[] = $mission;
        }

        return $result;
    }

    public function add($mission) {
        $stmt = $this->getConnexion()->prepare('INSERT INTO mission VALUES 
                                                (:code, 
                                                :nom, 
                                                :descr, 
                                                :codest, 
                                                :codespe, 
                                                :codepays, 
                                                :codetm, 
                                                -- :nbragent, 
                                                -- :nbrcontact, 
                                                -- :nbrcible, 
                                                -- :nbrplanque, 
                                                :dated, 
                                                :datef);');
                                                
        $stmt->execute(['code' => NULL,
                        'nom' => $mission->getNom(),
                        'descr' => $mission->getDescription(),
                        'codest' => $mission->getStatutmission(),
                        'codespe' => $mission->getSpecialite(),
                        'codepays' => $mission->getPays(),
                        'codetm' => $mission->getTypemission(),
                        // 'nbragent' => $mission->getNbragent(),
                        // 'nbrcontact' => $mission->getNbrcontact(),
                        // 'nbrcible' => $mission->getNbrcible(),
                        // 'nbrplanque' => $mission->getNbrplanque(),
                        'dated' => $mission->getDatedebut(),
                        'datef' => $mission->getDatefin()]);
        return true;
    }

    public function  delete($mission) {
        $stmt = $this->getConnexion()->prepare('DELETE FROM mission WHERE code_mission = :code');

        $result = $stmt->execute(['code' => $mission->getCode()]);

        return $result;
    }
}