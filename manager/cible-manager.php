<!-- MANAGER CIBLE -->

<?php

require dirname(__DIR__).'/autoloader.php';
require dirname(__DIR__).'/modele/cibles.php';

// CLASS CIBLE
class CibleManager extends DBManager{

    public function getAll() {
        $result = [];

        $stmt = $this->getConnexion()->query('SELECT * FROM cible JOIN pays ON cible.code_pays = pays.code_pays');

        while($row = $stmt->fetch()) {
            $cible = new Cible();
            $cible->setCode($row['code_cible']);
            $cible->setNom($row['nom_cible']);
            $cible->setPrenom($row['prenom_cible']);
            $cible->setDatenaissance($row['date_naissance_cible']);
            $cible->setCodepays($row['code_pays']);
            // INFO FROM JOIN TABLE : PAYS
            $cible->setNamePays($row['pays']);

            $result[] = $cible;
        }

        return $result;
    }

    public function add($cible) {
        $stmt = $this->getConnexion()->prepare('INSERT INTO cible VALUES 
                                                (:code, 
                                                :nom, 
                                                :prenom, 
                                                :datena, 
                                                :codepays);');
                                                
        $stmt->execute(['code' => NULL,
                        'nom' => $cible->getNom(),
                        'prenom' => $cible->getPrenom(),
                        'datena' => $cible->getDatenaissance(),
                        'codepays' => $cible->getCodepays()]);
        return true;
    }

    public function delete($cible) {

        $stmt = $this->getConnexion()->prepare('DELETE FROM cible WHERE code_cible = :code');

        $result = $stmt->execute(['code' => $cible->getCode()]);

        return $result;
    }
}