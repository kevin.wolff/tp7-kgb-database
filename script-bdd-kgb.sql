START TRANSACTION;

-- ============================================================
--   Suppression et création de la base de données 
-- ============================================================

DROP DATABASE IF EXISTS kgbdb;
CREATE DATABASE kgbdb;
USE kgbdb;


-- ============================================================
--   Création des tables                                
-- ============================================================


CREATE TABLE type_de_planque (
    code_planque INT AUTO_INCREMENT,
    type_planque VARCHAR(255) NOT NULL,
    PRIMARY KEY(code_planque)
);

CREATE TABLE type_mission (
    code_type_mission INT AUTO_INCREMENT,
    type_mission VARCHAR(100) NOT NULL,
    PRIMARY KEY(code_type_mission)
);

CREATE TABLE pays (
    code_pays INT AUTO_INCREMENT,
    pays VARCHAR(100) NOT NULL,
    PRIMARY KEY(code_pays)
);

CREATE TABLE statut_mission (
    code_statut INT AUTO_INCREMENT,
    statut_de_mission VARCHAR(100) NOT NULL,
    PRIMARY KEY(code_statut)
);

CREATE TABLE specialite (
    code_spe INT AUTO_INCREMENT,
    specialite VARCHAR(100) NOT NULL,
    PRIMARY KEY(code_spe)
);

CREATE TABLE agent (
    code_agent INT AUTO_INCREMENT,
    nom_code VARCHAR(100) NOT NULL,
    nom_agent VARCHAR(100),
    prenom_agent VARCHAR(100),
    date_naissance_agent VARCHAR(100),
    code_spe INT NOT NULL,
    code_pays INT NOT NULL,

    PRIMARY KEY(code_agent),

    CONSTRAINT fk_agent_pays
    FOREIGN KEY(code_pays)
    REFERENCES pays(code_pays),

    CONSTRAINT fk_agent_specialite
    FOREIGN KEY(code_spe)
    REFERENCES specialite(code_spe)
);

CREATE TABLE cible (
    code_cible INT AUTO_INCREMENT,
    nom_cible VARCHAR(100) NOT NULL,
    prenom_cible VARCHAR(100) NOT NULL,
    date_naissance_cible VARCHAR(100),
    code_pays INT NOT NULL,
    PRIMARY KEY(code_cible),
    CONSTRAINT fk_cible_pays FOREIGN KEY(code_pays) REFERENCES pays(code_pays)
);

CREATE TABLE contact (
    code_contact INT AUTO_INCREMENT,
    nom_code VARCHAR(100) NOT NULL,
    nom_contact VARCHAR(100),
    prenom_contact VARCHAR(100),
    date_naissance_contact VARCHAR(100),
    code_pays INT NOT NULL,
    PRIMARY KEY(code_contact),
    CONSTRAINT fk_contact_pays FOREIGN KEY(code_pays) REFERENCES pays(code_pays)
);

CREATE TABLE planque (
    code_planque INT AUTO_INCREMENT,
    nom_code VARCHAR(100) NOT NULL,
    adresse_planque VARCHAR(255),
    code_pays INT NOT NULL,
    code_type INT NOT NULL,
    PRIMARY KEY(code_planque),
    CONSTRAINT fk_planque_pays FOREIGN KEY(code_pays) REFERENCES pays(code_pays),
    CONSTRAINT fk_planque_type_de_planque FOREIGN KEY(code_type) REFERENCES type_de_planque(code_planque)
);

CREATE TABLE administrateur (
    code_admin INT AUTO_INCREMENT,
    nom_admin VARCHAR(100),
    prenom_admin VARCHAR(100),
    email VARCHAR(255) NOT NULL,
    mdp VARCHAR(255) NOT NULL,
    date_creation VARCHAR(100),
    PRIMARY KEY(code_admin)
);

CREATE TABLE mission (
    code_mission INT AUTO_INCREMENT,
    nom_mission VARCHAR(255) NOT NULL,
    description_mission VARCHAR(255),
    nom_code_mission VARCHAR(100),
    code_statut INT NOT NULL,
    code_spe INT NOT NULL,
    code_pays INT NOT NULL,
    code_type_mission INT NOT NULL,
    nbr_agent INT NOT NULL,
    nbr_contact INT NOT NULL,
    nbr_cible INT NOT NULL,
    nbr_planque INT,
    date_debut VARCHAR(100) NOT NULL,
    date_fin VARCHAR(100),

    PRIMARY KEY(code_mission),

    CONSTRAINT fk_mission_statut
    FOREIGN KEY (code_statut)
    REFERENCES statut_mission(code_statut),

    CONSTRAINT fk_mission_specialite
    FOREIGN KEY (code_spe)
    REFERENCES specialite(code_spe),

    CONSTRAINT fk_mission_pays
    FOREIGN KEY (code_pays)
    REFERENCES pays(code_pays),

    CONSTRAINT fk_mission_type_mission
    FOREIGN KEY (code_type_mission)
    REFERENCES type_mission(code_type_mission)
);


CREATE TABLE mission_necessite_contact (
    code_mission INT,
    code_contact INT,
    PRIMARY KEY(code_mission, code_contact),
    CONSTRAINT fk_mission_contact FOREIGN KEY (code_mission) REFERENCES mission(code_mission),
    CONSTRAINT fk_contact_mission FOREIGN KEY (code_contact) REFERENCES contact(code_contact)
);

CREATE TABLE mission_utilise_planque (
    code_mission INT,
    code_planque INT,
    PRIMARY KEY(code_mission, code_planque),
    CONSTRAINT fk_mission_planque FOREIGN KEY (code_mission) REFERENCES mission(code_mission),
    CONSTRAINT fk_planque_mission FOREIGN KEY (code_planque) REFERENCES planque(code_planque)
);

CREATE TABLE mission_attribut_spe (
    code_mission INT,
    code_spe INT,
    PRIMARY KEY (code_mission, code_spe),
    CONSTRAINT fk_mission_spe FOREIGN KEY(code_mission) REFERENCES mission(code_mission),
    CONSTRAINT fk_spe_mission FOREIGN KEY(code_spe) REFERENCES specialite(code_spe)
);

CREATE TABLE mission_vise_cible (
    code_mission INT,
    code_cible INT,
    PRIMARY KEY(code_mission, code_cible),
    CONSTRAINT fk_mission_cible FOREIGN KEY(code_mission) REFERENCES mission(code_mission),
    CONSTRAINT fk_cible_mission FOREIGN KEY(code_cible) REFERENCES cible(code_cible)
);

CREATE TABLE agent_possede_spe (
    code_agent INT,
    code_spe INT,
    PRIMARY KEY(code_agent, code_spe),
    CONSTRAINT fk_agent_spe FOREIGN KEY(code_agent) REFERENCES agent(code_agent),
    CONSTRAINT fk_spe_agent FOREIGN KEY(code_spe) REFERENCES specialite(code_spe)
);

-- ============================================================
--   Insertion des enregistrements
-- ============================================================

INSERT INTO administrateur VALUES(NULL, "admin", "admin", "admin@admin.fr", "$2y$12$x1cQK3U.XjmRJ7cUSCnUqebFNiCStEX.bJH8ClChHooGSD0XbIcQS", "2020-01-01");

INSERT INTO `pays` (`code_pays`, `pays`) VALUES (NULL, 'Angleterre'), (NULL, 'Ukraine'), (NULL, 'France');
INSERT INTO `specialite` (`code_spe`, `specialite`) VALUES (NULL, 'Tourmenter'), (NULL, 'Effrayer'), (NULL, 'Moquer');
INSERT INTO `statut_mission` (`code_statut`, `statut_de_mission`) VALUES (NULL, 'En cours'), (NULL, 'En pause'), (NULL, 'Achever');
INSERT INTO `type_de_planque` (`code_planque`, `type_planque`) VALUES (NULL, 'Appartement'), (NULL, 'Villa'), (NULL, 'Camping');
INSERT INTO `type_mission` (`code_type_mission`, `type_mission`) VALUES (NULL, 'Purifier'), (NULL, 'Espionner'), (NULL, 'Ennuyer');
INSERT INTO `agent` (`code_agent`, `nom_code`, `nom_agent`, `prenom_agent`, `date_naissance_agent`, `code_spe`, `code_pays`) VALUES (NULL, 'Empereur', 'Alka', 'Pote', '01/01/0001', '1', '1'), (NULL, 'La vermine', 'Bernard', 'Tapie', '01/01/1941', '2', '3'), (NULL, 'Magle', 'Hillary', 'Clinton', '25/12/1992', '3', '2');
INSERT INTO `cible` (`code_cible`, `nom_cible`, `prenom_cible`, `date_naissance_cible`, `code_pays`) VALUES (NULL, 'Krusty', 'Bowl', '29/06/1962', '3'), (NULL, 'Amy', 'Winehouse', '08/09/2002', '1'), (NULL, 'Bojack', 'Horseman', '18/01/1981', '2');
INSERT INTO `contact` (`code_contact`, `nom_code`, `nom_contact`, `prenom_contact`, `date_naissance_contact`, `code_pays`) VALUES (NULL, 'La pookie', 'Sixty', 'Nine', '22/02/1979', '2'), (NULL, 'Affreux', 'Jean-marie', 'Le borgne', '11/09/1800', '1'), (NULL, 'Le chacal', 'Jok', 'Air', '15/03/1979', '1');
INSERT INTO `mission` (`code_mission`, `nom_mission`, `description_mission`, `nom_code_mission`, `code_statut`, `code_spe`, `code_pays`, `code_type_mission`, `nbr_agent`, `nbr_contact`, `nbr_cible`, `nbr_planque`, `date_debut`, `date_fin`) VALUES (NULL, 'Le grand tourment', 'Vous devrez purifier l\'âme de la cible en la tourmentant grâce a votre Tenefix', 'Code Yoko', '1', '1', '3', '1', '1', '1', '1', '1', '01/01/2020', '01/01/2021'), (NULL, 'Tout pour rire', 'Vous devrez humilier la cible à l\'aide des meilleurs répliques de Bigard', 'Gros beauf', '3', '3', '2', '3', '1', '1', '1', '1', '19/06/2008', '25/12/2017');
INSERT INTO `planque` (`code_planque`, `nom_code`, `adresse_planque`, `code_pays`, `code_type`) VALUES (NULL, 'La villa', 'Rue de Rivoli', '1', '3'), (NULL, 'Orgie', 'Cours berriat', '3', '2'), (NULL, 'Le four', 'Quartier le fond vert', '2', '1');

-- ============================================================
--   Validation de la transaction
-- ============================================================

COMMIT;