<?php
    require_once __DIR__ . '/manager/DB-manager.php';
    require_once __DIR__ . '/manager/pays-manager.php';
    require_once __DIR__ . '/manager/specialite-manager.php';
    require_once __DIR__ . '/manager/type-mission-manager.php';
    require_once __DIR__ . '/manager/statut-mission-manager.php';
    require_once __DIR__ . '/manager/type-planque-manager.php';
    require_once __DIR__ . '/manager/mission-manager.php';
    require_once __DIR__ . '/manager/agent-manager.php';
    require_once __DIR__ . '/manager/cible-manager.php';
    require_once __DIR__ . '/manager/contact-manager.php';
    require_once __DIR__ . '/manager/planque-manager.php';
?>