<!-- DB CONNEXION -->
<?php
try
{
    $bdd = new PDO('');
}
catch (Exception $e)
{
    die('Erreur : ' . $e->getMessage());
}

session_start();

require './autoloader.php';

?>

<!-- HTML -->
<!DOCTYPE html>
<html lang='en'>
<head>
    <meta charset='UTF-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <title>Projet-kgb</title>
    <!-- CUSTOM STYLESHEET -->
    <link rel='stylesheet' href='assets/dist/style.css'>
    <!-- CUSTOM FONT -->
    <link href="https://fonts.googleapis.com/css2?family=Graduate&family=Source+Code+Pro&display=swap" rel="stylesheet">
</head>
<body>

    <!-- NAVBAR -->
    <?php include ("./assets/php/navbar.php");?>

    <?php 
    // FORM IF NOT LOGED
    if(!isset($_SESSION['code_admin']))
    {
    ?>
        <!-- CONNEXION FORM -->
        <form method="post" action="assets/exe/exe-connexion.php" class="connexion-form">
            <label for="mail">Adresse mail *</label>
            <input type="email" name="mail">
            <label for="pass">Mot de passe *</label>
            <input type="password" name="pass">
            <p class="obligatoire">* champs obligatoire</p>
            <input type="submit" value="connexion" class="button">
        </form>
    <?php
    }
    // ADMINISTRATION MENU IF LOGED
    else
    {
    ?>
        <!-- HOME MSG -->
        <div class="page-message">
            <h2>_Bienvenue <?= $_SESSION['prenom_admin'] . ' ' . $_SESSION['nom_admin'];?></h2>
        </div>
        <div class="box-anim"></div>
        
        <!-- ADMIN INTERFACE -->
        <!-- WRAPPER -->
        <div class="admin-card" id="admin-card">

            <!-- ADMINISTRATION CARD -->
            <div class="form-card">
                <div class="card-unclick">
                    <?php include ("./assets/png/png-administration.php");?>
                    <p>Administration</p>
                </div>
                <div class="card-click">
                    <?php include ("./assets/png/png-close.php");?>
                    <div class="card-click-title">
                        <?php include ("./assets/png/png-administration.php");?>
                        <p>Nouvel administrateur</p>
                    </div>
                    <!-- ADMINISTRATION FORM -->
                    <?php include ("./assets/php/admin-form.php");?>
                </div>
            </div>

            <!-- MISSION CARD -->
            <div class="form-card">
                <div class="card-unclick">
                    <?php include ("./assets/png/png-mission.php");?>
                    <p>Mission</p>
                </div>
                <div class="card-click">
                <?php include ("./assets/png/png-close.php");?>
                    <div class="card-click-title">
                        <?php include ("./assets/png/png-mission.php");?>
                        <p>Nouvelle mission</p>
                    </div>
                    <!-- MISSION FORM -->
                    <?php include ("./assets/php/mission-form.php");?>
                </div>
            </div>

            <!-- TYPE DE MISSION CARD -->
            <div class="form-card">
                <div class="card-unclick">
                    <?php include ("./assets/png/png-type-mission.php");?>
                    <p>Type mission</p>
                </div>
                <div class="card-click">
                <?php include ("./assets/png/png-close.php");?>
                    <div class="card-click-title">
                        <?php include ("./assets/png/png-type-mission.php");?>
                        <p>Nouveau type</p>
                    </div>
                    <!-- TYPE DE MISSION FORM -->
                    <?php include ("./assets/php/type-mission-form.php");?>
                </div>
            </div>

            <!-- STATUT MISSION CARD -->
            <div class="form-card">
                <div class="card-unclick">
                    <?php include ("./assets/png/png-statut-mission.php");?>
                    <p>Statut mission</p>
                </div>
                <div class="card-click">
                <?php include ("./assets/png/png-close.php");?>
                    <div class="card-click-title">
                        <?php include ("./assets/png/png-statut-mission.php");?>
                        <p>Nouveau statut</p>
                    </div>
                    <!-- STATUT MISSION FORM -->
                    <?php include ("./assets/php/statut-mission-form.php");?>
                </div>
            </div>

            <!-- PAYS CARD -->
            <div class="form-card">
                <div class="card-unclick">
                    <?php include ("./assets/png/png-pays.php");?>
                    <p>Pays</p>
                </div>
                <div class="card-click">
                <?php include ("./assets/png/png-close.php");?>
                    <div class="card-click-title">
                        <?php include ("./assets/png/png-pays.php");?>
                        <p>Nouveau pays</p>
                    </div>
                    <!-- PAYS FORM -->
                    <?php include ("./assets/php/pays-form.php");?>
                </div>
            </div>

            <!-- AGENT CARD -->
            <div class="form-card">
                <div class="card-unclick">
                    <?php include ("./assets/png/png-agent.php");?>
                    <p>Agent</p>
                </div>
                <div class="card-click">
                <?php include ("./assets/png/png-close.php");?>
                    <div class="card-click-title">
                        <?php include ("./assets/png/png-agent.php");?>
                        <p>Nouvel agent</p>
                    </div>
                    <!-- AGENT FORM -->
                    <?php include ("./assets/php/agent-form.php");?>
                </div>
            </div>

            <!-- SPECIALITE CARD -->
            <div class="form-card">
                <div class="card-unclick">
                    <?php include ("./assets/png/png-specialite.php");?>
                    <p>Spécialité</p>
                </div>
                <div class="card-click">
                <?php include ("./assets/png/png-close.php");?>
                    <div class="card-click-title">
                        <?php include ("./assets/png/png-specialite.php");?>
                        <p>Nouvelle spécialité</p>
                    </div>
                    <!-- SPECIALITE FORM -->
                    <?php include ("./assets/php/specialite-form.php");?>
                </div>
            </div>

            <!-- CIBLE CARD -->
            <div class="form-card">
                <div class="card-unclick">
                    <?php include("./assets/png/png-cible.php");?>
                    <p>Cible</p>
                </div>
                <div class="card-click">
                <?php include ("./assets/png/png-close.php");?>
                    <div class="card-click-title">
                        <?php include("./assets/png/png-cible.php");?>
                        <p>Nouvelle cible</p>
                    </div>
                    <!-- CIBLE FORM -->
                    <?php include ("./assets/php/cible-form.php");?>
                </div>
            </div>

            <!-- CONTACT CARD -->
            <div class="form-card">
                <div class="card-unclick">
                    <?php include("./assets/png/png-contact.php");?>
                    <p>Contact</p>
                </div>
                <div class="card-click">
                <?php include ("./assets/png/png-close.php");?>
                    <div class="card-click-title">
                        <?php include("./assets/png/png-contact.php");?>
                        <p>Nouveau contact</p>
                    </div>
                    <!-- CONTACT FORM -->
                    <?php include ("./assets/php/contact-form.php");?>
                </div>
            </div>

            <!-- PLANQUE CARD -->
            <div class="form-card">
                <div class="card-unclick">
                    <?php include("./assets/png/png-planque.php");?>
                    <p>Planque</p>
                </div>
                <div class="card-click">
                <?php include ("./assets/png/png-close.php");?>
                    <div class="card-click-title">
                        <?php include("./assets/png/png-planque.php");?>
                        <p>Nouvelle planque</p>
                    </div>
                    <!-- PLANQUE FORM -->
                    <?php include ("./assets/php/planque-form.php");?>
                </div>
            </div>

            <!-- TYPE DE PLANQUE CARD -->
            <div class="form-card">
                <div class="card-unclick">
                    <?php include("./assets/png/png-type-planque.php");?>
                    <p>Type planque</p>
                </div>
                <div class="card-click">
                <?php include ("./assets/png/png-close.php");?>
                    <div class="card-click-title">
                        <?php include("./assets/png/png-type-planque.php");?>
                        <p>Nouveau type</p>
                    </div>
                    <!-- TYPE DE PLANQUE FORM -->
                    <?php include ("./assets/php/type-planque-form.php");?>
                </div>
            </div>
        </div>
    <?php
    }
    ?>

    <!-- CUSTOM JS SCRIPT -->
    <script src='assets/js/admin-script.js'></script>
    <script src='assets/js/navbar-toggle.js'></script>
</body>
</html>
