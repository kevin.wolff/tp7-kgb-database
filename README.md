# TP7 - Base de donnée du KGB

**Projet en binome avec [Guillaume Buisson](https://gitlab.com/guillaume.buisson.simplon)**

**A rendre pour le 25/09/2020 a 17:30**

***Note d'utilisation*** :

*Pour faire fonctionner le site vous devrez executer le script SQL disponible dans "script-bdd-kgb.sql" pour créer la BDD. Vous devrez aussi modifier la requête PDO des fichiers suivants :*

- *index.php*
- *administration.php*
- *assets/exe/exe_connexion.php*
- *assets/exe/exe_deconnexion.php*
- *manager/DB-manager.php*

*Pour accéder à la page "administration.php" vous devrez vous connecter, un identifiant est déjà present dans le script de la BDD, à savoir : admin@admin.fr - 123. Enjoy!*

### Consignes :

Vous devez créer la base de données selon la description *Voir MLD plus bas*. Tous les champs devront avoir les bons types, avec optimisation. Il faut également créer les liens entre les différentes tables. Certaines colonnes sont peut être manquantes et nécessaire à votre développement, à vous de les fournir. Aucun jeu de données n’est fourni.

Vous devez créer une interface front-office, accessible à tous, permettant de consulter la liste de toutes les missions, ainsi qu’une page permettant de voir le détail d’une mission.

De plus, il faudra créer une interface back-office, uniquement accessible aux utilisateurs de rôle ADMIN, qui va permettre de gérer la base de données de la bibliothèque. Ce back-office va permettre de lister, créer, modifier et supprimer chaque données des différentes tables, grâce à des formulaires et des tableaux. Il faut que ces pages ne soient pas accessibles à tout le monde !

**Points obligatoires :**

- Réaliser en HTML5, SASS, JS ES6+, PHP7, MySQL
- Réaliser un schema MCD/MLD de la base de donnée
- Créer un script de création de la base de données, facilement exécutable
- Créer une page de connexion et de deconnexion (pas de page d'inscription) pour l'acces à l'interface back-end
- Réaliser le projet en programmation orienté objet, de type MVC. Chaque table de la base de donnée sera représentée par un objet PHP

**MLD base de données :**

**Administrateur** [<u>code_admin</u>, nom_admin, prenom_admin, email, mdp, date_creation]
Clé primaire (code_admin)

**Mission** [<u>code_mission</u>, nom_mission, description_mission, nom_code_mission, code_statut, code_spe, code_pays, code_type_mission, nbr_agent, nbr_contact, nbr_cible, nbr_planque, date_debut, date_fin]
Clé primaire (code_mission)
Clé étrangère (code_statut, code_spe, code_pays, code_type_mission, code_agent, code_cible, code_contact, code_planque)

**Agent** [<u>code_agent</u>, nom_code, nom_agent, prenom_agent, date_naissance_agent, code_pays, code_spe]
Clé primaire (code_agent)
Clé étrangère (code_pays, code_spé)

**Cible** [<u>code_cible</u>, nom_cible, prenom_cible, date_naissance_cible, code_pays]
Clé primaire (code_cible)
Clé étrangère (code_pays)

**Contact** [<u>code_contact</u>, nom_code, nom_contact, prenom_contact, date_naissance_contact, code_pays]
Clé primaire (code_contact)
Clé étrangère (code_pays)

**Planque** [<u>code_planque</u>, nom_code, adresse_planque, code_pays, code_type]
Clé primaire (code_planque)
Clé étrangère (code_pays, code_type)

**Pays** [<u>code_pays</u>, pays]
Clé primaire (code_pays)

**Spécialité** [<u>code_specialite</u>, specialite]
Clé primaire (code_specialite)

**Type de planque** [<u>code_planque</u>, type_planque]
Clé primaire (code_planque)

**Type de mission** [<u>code_mission</u>, type_mission]
Clé primaire (code_mission)

**Statut mission** [<u>code_statut</u>, statut_de_mission]
Clé primaire (code_statut)