// SCRIPT BURGER MENU DISPLAY ON CLICK

const menuBtn = document.getElementById("toggle-btn"); // Variable toggle burger button
const closeBtn = document.getElementById("close-btn"); // Variable toggle close button
const menuToggle = document.getElementById("menu-toggle"); // Variable div menu-toggle

// On click ajoute / retire la class active de la div menu-toggle
menuBtn.addEventListener("click", function() {

    menuToggle.classList.add("active");
    menuBtn.classList.add("active");
    closeBtn.classList.add("active");
})

closeBtn.addEventListener("click", function() {
    
    menuToggle.classList.remove("active");
    menuBtn.classList.remove("active");
    closeBtn.classList.remove("active");
})