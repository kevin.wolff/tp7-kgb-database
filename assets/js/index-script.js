// SCRIPT MISSION CARD DISPLAY ON CLICK

const allMissionUnclick = document.querySelectorAll(".mission-unclick"); // Variable all mission-unclik

// Pour chaque div mission-card ajoute un eventListener
for (let missionUnclick of allMissionUnclick) {

    missionUnclick.addEventListener("click", function() {

        const divMissionCard = missionUnclick.closest("#mission-cards"); // Selectionne la div #admin-card

        const allDivMissionCard = divMissionCard.querySelectorAll(".mission-card"); // Selectionne toutes les div mission-card
        // Retire la class clicked a toutes les div mission-card
        for (let divMissionCard of allDivMissionCard) {
            divMissionCard.classList.remove("clicked");
        }
        const targetDivMissionCard = missionUnclick.parentNode; // Selectionne la mission-card de l'élément ciblé
        // Attribue la class clicked à l'élément ciblé
        targetDivMissionCard.classList.add("clicked");

        for (let missionUnclick of allMissionUnclick) {
            missionUnclick.classList.remove("clicked");
        }
        missionUnclick.classList.add("clicked");

        const allDivMissionClick = divMissionCard.querySelectorAll(".mission-click"); // Selectionne toutes les div mission-click
        // Retire la class clicked a toutes les div card-click
        for (let divMissionClick of allDivMissionClick) {
            divMissionClick.classList.remove("clicked");
        }
        const targetDivMissionClick = targetDivMissionCard.querySelector(".mission-click"); // Selectionne la mission-click de l'élément ciblé
        // Attribue la class clicked à l'élément ciblé
        targetDivMissionClick.classList.add("clicked");
        
    })
}

// SCRIPT TOOL CARD DISPLAY ON CLICK

const allToolUnclik = document.querySelectorAll(".tool-unclick"); // Variable all tool-unclik

// Pour chaque div tool-card ajoute un eventListener
for (let toolUnclik of allToolUnclik) {

    toolUnclik.addEventListener("click", function() {

        const divToolCard = toolUnclik.closest("#tool-cards"); // Selectionne la div #tool-cards

        const allDivToolCard = divToolCard.querySelectorAll(".tool-card"); // Selectionne toutes les div tool-card
        // Retire la class clicked a toutes les div tool-card
        for (let divToolCard of allDivToolCard) {
            divToolCard.classList.remove("clicked");
        }
        const targetDivToolCard = toolUnclik.parentNode; // Selectionne la tool-card de l'élément ciblé
        // Attribue la class clicked à l'élément ciblé
        targetDivToolCard.classList.add("clicked");

        for (let toolUnclik of allToolUnclik) {
            toolUnclik.classList.remove("clicked");
        }
        toolUnclik.classList.add("clicked");

        const allDivToolClick = divToolCard.querySelectorAll(".tool-click"); // Selectionne toutes les div card-click
        // Retire la class clicked a toutes les div card-click
        for (let divToolClick of allDivToolClick) {
            divToolClick.classList.remove("clicked");
        }
        const targetDivToolClick = targetDivToolCard.querySelector(".tool-click"); // Selectionne la card-click de l'élément ciblé
        // Attribue la class clicked à l'élément ciblé
        targetDivToolClick.classList.add("clicked");
    })
}

////////////////////////////////////////////////////////////////////////////////////////

const divMissionCards = document.getElementById("mission-cards"); // Variable de la div parent des mission-card
const divToolCards = document.getElementById("tool-cards"); // Variable de la div parent des tool-card

const missionButtons = divMissionCards.querySelectorAll(".card-close-btn"); // Variable all mission card-close-button
const toolButtons = divToolCards.querySelectorAll(".card-close-btn"); // Variable all toll card-close-button

// SCRIPT MISSION CARDS CLOSE WHEN CLICK ON BUTTON

// Pour chaque button ajoute un eventListener
for (let button of missionButtons) {

    // On click retire "clicked" à mission-card, div card-unclick et div card-click
    button.addEventListener("click", function(e) {

        if (e.target != this) {

        }
        else
        {            
            //  HIDE DIV MISSION-CARD
            let allDivClassMissionCard = divMissionCards.querySelectorAll(".mission-card"); // Variable all div mission-unclick

            // Pour chaque div mission-card retire la class "clicked"
            for (divClassMissionCard of allDivClassMissionCard) {
                divClassMissionCard.classList.remove("clicked");
            }

            //  HIDE DIV MISSION-UNCLICK
            let allDivClassMissionUnclick = divMissionCards.querySelectorAll(".mission-unclick"); // Variable all div mission-unclick

            // Pour chaque div mission-unclick retire la class "clicked"
            for (divClassMissionUnclick of allDivClassMissionUnclick) {
                divClassMissionUnclick.classList.remove("clicked");
            }

            // HIDE DIV MISSION-CLICK
            let allDivClassMissionClick = divMissionCards.querySelectorAll(".mission-click"); // Variable all div mission-unclick

            // Pour chaque div mission-unclick retire la class "clicked"
            for (divClassMissionClick of allDivClassMissionClick) {
                divClassMissionClick.classList.remove("clicked");
            }
        }
    })
}

// SCRIPT TOOL CARDS CLOSE WHEN CLICK ON BUTTON

// Pour chaque button ajoute un eventListener
for (let button of toolButtons) {

    // On click retire "clicked" à tool-card, div tool-unclick et div tool-click
    button.addEventListener("click", function(e) {

        if (e.target != this) {
        }
        else
        {            
            //  HIDE DIV TOOL-CARD
            let allDivClassToolCard = divToolCards.querySelectorAll(".tool-card"); // Variable all div tool-unclick

            // Pour chaque div tool-card retire la class "clicked"
            for (divClassToolCard of allDivClassToolCard) {
                divClassToolCard.classList.remove("clicked");
            }

            //  HIDE DIV TOOL-UNCLICK
            let allDivClassToolUnclick = divToolCards.querySelectorAll(".tool-unclick"); // Variable all div tool-unclick

            // Pour chaque div tool-unclick retire la class "clicked"
            for (divClassToolUnclick of allDivClassToolUnclick) {
                divClassToolUnclick.classList.remove("clicked");
            }

            // HIDE DIV TOOL-CLICK
            let allDivClassToolClick = divToolCards.querySelectorAll(".tool-click"); // Variable all div tool-unclick

            // Pour chaque div tool-unclick retire la class "clicked"
            for (divClassToolClick of allDivClassToolClick) {
                divClassToolClick.classList.remove("clicked");
            }
        }
    })
}