<!-- FORMULAIRE AGENT -->

<?php
    $mypaysmanager = new PaysManager();
    $myallpays = $mypaysmanager->getAll();

    $myspecialitemanager = new SpecialiteManager();
    $myallspecialite = $myspecialitemanager->getAll();
?>

<form method="POST" action="./assets/exe/exe-agent-form.php">
    <label for="nom-code">Nom de code *</label>
    <input type="text" name="nom-code" required>
    <label for="nom">Nom</label>
    <input type="text" name="nom">
    <label for="prenom">Prenom</label>
    <input type="text" name="prenom">
    <label for="date_naissance">Date naissance</label>
    <input type="date" name="date_naissance" class="date-naissance">
    <div class="select-wrapper">
        <div class="select-box">
            <label for="pays">Pays</label>
            <select name="pays">
                <?php foreach($myallpays as $mypays) 
                {
                ?>
                    <option value="<?= $mypays->getCodepays(); ?>"><?= $mypays->getPays(); ?></option>
                <?php
                }
                ?>
            </select>
        </div>
        <div>
            <div class="select-box">
                <label for="specialite">Specialite</label>
                <select name="specialite">
                    <?php foreach($myallspecialite as $myspecialite) 
                    {
                    ?>
                        <option value="<?= $myspecialite->getCode(); ?>"><?= $myspecialite->getSpecialite(); ?></option>
                    <?php
                    }
                    ?>
                </select>
            </div>
        </div>
    </div>
    <p class="obligatoire">* champs obligatoire</p>
    <input type="submit" value="Enregistrer" class="button">
</form>