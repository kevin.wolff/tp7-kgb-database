<!-- FORMULAIRE MISSION -->

<?php
    $mytypemanager = new TypeMissionManager();
    $myalltype = $mytypemanager->getAll();

    $mystatutmanager = new StatutMissionManager();
    $myallstatut = $mystatutmanager->getAll();

    $mypaysmanager = new PaysManager();
    $myallpays = $mypaysmanager->getAll();

    $myspecialitemanager = new SpecialiteManager();
    $myallspecialite = $myspecialitemanager->getAll();
?>

<form method="POST" action="./assets/exe/exe-mission-form.php">
    <label for="code">Nom de code mission</label>
    <input type="text" name="code">
    <label for="nom">Nom mission *</label>
    <input type="text" name="nom" required>
    <label for="description">Description mission</label>
    <input type="text" name="description">
    <div class="select-wrapper">
        <div class="select-box">
            <label for="type">Type *</label>
            <select name="type">
                <?php foreach($myalltype as $mytype) 
                {
                ?>
                    <option value="<?= $mytype->getCodetypemission(); ?>"><?= $mytype->getTypemission(); ?></option>
                <?php
                }
                ?>
            </select>
        </div>
        <div>
            <div class="select-box">
                <label for="statut">Statut *</label>
                <select name="statut">
                    <?php foreach($myallstatut as $mystatut) 
                    {
                    ?>
                        <option value="<?= $mystatut->getCodestatut(); ?>"><?= $mystatut->getStatutmission(); ?></option>
                    <?php
                    }
                    ?>
                </select>
            </div>
        </div>
    </div>
    <div class="select-wrapper">
        <div class="select-box">
            <label for="pays">Pays *</label>
            <select name="pays">
                <?php foreach($myallpays as $mypays) 
                {
                ?>
                    <option value="<?= $mypays->getCodepays(); ?>"><?= $mypays->getPays(); ?></option>
                <?php
                }
                ?>
            </select>
        </div>
        <div class="select-box">
            <label for="specialite">Specialite *</label>
            <select name="specialite">
                <?php foreach($myallspecialite as $myspecialite) 
                {
                ?>
                    <option value="<?= $myspecialite->getCode(); ?>"><?= $myspecialite->getSpecialite(); ?></option>
                <?php
                }
                ?>
            </select>
        </div>
    </div>
    <div class="select-wrapper">
        <div class="select-box">
            <label for="debut">Début de la mission *</label>
            <input type="date" name="debut" class="date" required>
        </div>
        <div>
            <div class="select-box">
                <label for="fin">Fin de la mission</label>
                <input type="date" name="fin" class="date" required>
            </div>
        </div>
    </div>
    <!-- NOMBRE_AGENT ? -->
    <!-- NOMBRE_CONTACT ? -->
    <!-- NOMBRE_CIBLE ? -->
    <!-- NOMBRE_PLANQUE ? -->
    <p class="obligatoire">* champs obligatoire</p>
    <input type="submit" value="Enregistrer" class="button">
</form>