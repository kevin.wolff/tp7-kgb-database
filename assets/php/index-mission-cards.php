<!-- MISSIONS CARD -->
<div class="mission-cards" id="mission-cards">

    <!-- MISSION CARD -->
    <div class="mission-card">
        <div class="mission-unclick">
            <?php include ("./assets/png/png-mission.php");?>
            <p>Les missions</p>
        </div>
        <div class="mission-click">
            <?php include ("./assets/png/png-close.php");?>
            <?php foreach($myallmission as $mymission) 
            {
            ?>
            <div class="card-info">
                <h3><?= $mymission->getNomcode(); ?></h3>
                <p><?= $mymission->getNom(); ?></p>
                <p><?= $mymission->getDescription(); ?></p>
                <p><span class="underline">Type de mission</span> : <?= $mymission->getNameType(); ?></p>
                <p><span class="underline">Spécialité requise</span> : <?= $mymission->getNameSpe(); ?></p>
                <p><span class="underline">Statut</span> : <?= $mymission->getNameStatut(); ?></p>
                <p><span class="underline">Pays</span> : <?= $mymission->getNamePays(); ?></p>
                <?php 
                if(isset($_SESSION['code_admin'])) {
                    ?>
                    <a href="assets/exe/exe-remove-content.php?id=<?= $mymission->getCode() ?>&table=mission"><?php include('./assets/png/png-supprimer.php'); ?></a>
                    <?php
                }
                ?>
            </div>
            <?php
            }
            ?>
        </div>
    </div>

    <!-- AGENT CARD -->
    <div class="mission-card">
        <div class="mission-unclick">
            <?php include ("./assets/png/png-agent.php");?>
            <p>Nos agents</p>
        </div>
        <div class="mission-click">
            <?php include ("./assets/png/png-close.php");?>
            <?php foreach($myallagent as $myagent) 
            {
            ?>
            <div class="card-info">
                <h3><?= $myagent->getNomcode(); ?></h3>
                <p>Agent : <?= $myagent->getNom(); ?>, <?= $myagent->getPrenom(); ?></p>
                <p><?= $myagent->getDatenaissance(); ?> - <?= $myagent->getNamePays(); ?></p>
                <p>Spécialité : <?= $myagent->getNameSpe(); ?></p>
                <?php 
                if(isset($_SESSION['code_admin'])) {
                    ?>
                    <a href="assets/exe/exe-remove-content.php?id=<?= $myagent->getCode() ?>&table=agent"><?php include('./assets/png/png-supprimer.php'); ?></a>
                    <?php
                }
                ?>
            </div>
            <?php
            }
            ?>
        </div>
    </div>

    <!-- CIBLE CARD -->
    <div class="mission-card">
        <div class="mission-unclick">
            <?php include("./assets/png/png-cible.php");?>
            <p>Les cibles</p>
        </div>
        <div class="mission-click">
            <?php include ("./assets/png/png-close.php");?>
            <?php foreach($myallcible as $mycible) 
            {
            ?>
            <div class="card-info">
                <h3>Cible - <?= $mycible->getNom(); ?>, <?= $mycible->getPrenom(); ?></h3>
                <p><?= $mycible->getDatenaissance(); ?> - <?= $mycible->getNamepays(); ?></p>
                <?php 
                if(isset($_SESSION['code_admin'])) {
                    ?>
                    <a href="assets/exe/exe-remove-content.php?id=<?= $mycible->getCode() ?>&table=cible"><?php include('./assets/png/png-supprimer.php'); ?></a>
                    <?php
                }
                ?>
            </div>
            <?php
            }
            ?>
        </div>
    </div>

    <!-- CONTACT CARD -->
    <div class="mission-card">
        <div class="mission-unclick">
            <?php include("./assets/png/png-contact.php");?>
            <p>Nos contacts</p>
        </div>
        <div class="mission-click">
            <?php include ("./assets/png/png-close.php");?>
            <?php foreach($myallcontact as $mycontact) 
            {
            ?>
            <div class="card-info">
                <h3><?= $mycontact->getNomcode(); ?></h3>
                <p>Agent : <?= $mycontact->getNom(); ?>, <?= $mycontact->getPrenom(); ?></p>
                <p><?= $mycible->getDatenaissance(); ?> - <?= $mycontact->getNamePays(); ?></p>
                <?php 
                if(isset($_SESSION['code_admin'])) {
                    ?>
                    <a href="assets/exe/exe-remove-content.php?id=<?= $mycontact->getCode() ?>&table=contact"><?php include('./assets/png/png-supprimer.php'); ?></a>
                    <?php
                }
                ?>
            </div>
            <?php
            }
            ?>
        </div>
    </div>

    <!-- PLANQUE CARD -->
    <div class="mission-card">
        <div class="mission-unclick">
            <?php include("./assets/png/png-planque.php");?>
            <p>Nos planques</p>
        </div>
        <div class="mission-click">
            <?php include ("./assets/png/png-close.php");?>
            <?php foreach($myallplanque as $myplanque) 
            {
            ?>
            <div class="card-info">
                <h3>Nom code : <?= $myplanque->getNomcode(); ?></h3>
                <p><?= $myplanque->getAdresse(); ?> - <?= $myplanque->getNamePays(); ?></p>
                <p><?= $myplanque->getNameType(); ?></p>
                <?php 
                if(isset($_SESSION['code_admin'])) {
                    ?>
                    <a href="assets/exe/exe-remove-content.php?id=<?= $myplanque->getCode() ?>&table=planque"><?php include('./assets/png/png-supprimer.php'); ?></a>
                    <?php
                }
                ?>
            </div>
            <?php
            }
            ?>
        </div>
    </div>

</div>