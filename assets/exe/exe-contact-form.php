<!-- EXE FORMULAIRE CONTACT -->

<?php 

    require '../../manager/contact-manager.php';

    if (!empty($_POST['nom-code'])) {
        $mycontactmanager = new ContactManager();
    
        $newcontact = new Contact();
        $newcontact->setNomcode($_POST['nom-code']);
        $newcontact->setNom($_POST['nom']);
        $newcontact->setPrenom($_POST['prenom']);
        $newcontact->setDatenaissance((string)$_POST['date_naissance']);
        $newcontact->setCodepays((int)$_POST['pays']);
    
        $mycontactmanager->add($newcontact);
        header('location: ./../../loading-page.php');
    }
    else {
        header('location: ./../../administration.php');
    }