<!-- EXE FORMULAIRE CONNEXION -->

<!-- DB CONNEXION -->
<?php
try
{
    $bdd = new PDO('');
}
catch (Exception $e)
{
        die('Erreur : ' . $e->getMessage());
}

// USER CONNEXION

// Si formulaire rempli
if (!empty($_POST['mail']) && !empty($_POST['pass'])) {
    $mail = htmlspecialchars($_POST['mail']);
    $req = $bdd->prepare('SELECT * FROM administrateur WHERE email = :mail');
    $req->execute(array('mail' => $mail));
    $resultat = $req->fetch();
    
    // Si l'adresse mail ne match pas avec une adresse mail de la bdd
    if (empty($resultat))
    {
        header ('location: ./../../administration.php');
    }
    // Si l'adresse mail match avec une adresse mail de la bdd
    else
    {
        // Si mail OK et pass match
        if (password_verify($_POST['pass'], $resultat['mdp']))
        {
            session_start();
            $_SESSION['code_admin'] = $resultat['code_admin'];
            $_SESSION['nom_admin'] = $resultat['nom_admin'];
            $_SESSION['prenom_admin'] = $resultat['prenom_admin'];
            header ('location: ./../../administration.php');
        }
        // Si mail OK mais pass match pas
        else
        {
            header ('location: ./../../administration.php');
        }
    }
}
// Si formulaire pas rempli
else {
    header ('location: ./../../administration.php');
}
?>