<!-- EXE FORMULAIRE MISSION -->

<?php 

    require '../../manager/mission-manager.php';

    if (!empty($_POST['nom']) && !empty($_POST['type']) && !empty($_POST['statut']) && !empty($_POST['pays']) && !empty($_POST['specialite']) && !empty($_POST['debut'])) {
        $mymissionmanager = new MissionManager();

        $newmission = new Mission();
        $newmission->setNomcode($_POST['code']);
        $newmission->setTypemission($_POST['type']);
        $newmission->setNom($_POST['nom']);
        $newmission->setDescription($_POST['description']);
        $newmission->setTypemission((int)$_POST['type']);
        $newmission->setStatutmission((int)$_POST['statut']);
        $newmission->setPays((int)$_POST['pays']);
        $newmission->setSpecialite((int)$_POST['specialite']);
        $newmission->setNbragent('1');
        $newmission->setNbrcible('1');
        $newmission->setNbrcontact('1');
        $newmission->setNbrplanque('1');
        $newmission->setDatedebut((string)$_POST['debut']);
        $newmission->setDatefin((string)$_POST['fin']);
    
        $mymissionmanager->add($newmission);
        header('location: ./../../loading-page.php');
    }
    else {
        header('location: ./../../administration.php');
    }