<!-- EXE FORMULAIRE PAYS -->

<?php 
    require '../../manager/pays-manager.php';

    if (!empty($_POST['pays'])) {
        $mypaysmanager = new PaysManager();
    
        $newpays = new Pays();
        $newpays->setPays($_POST['pays']);
    
        $mypaysmanager->add($newpays);
        header ('location: ./../../loading-page.php');
    }
    else {
        header ('location: ./../../administration.php');
    }
    