<!-- EXE FORMULAIRE PLANQUE -->

<?php 
    require '../../manager/planque-manager.php';

    if (!empty($_POST['nom-code'])) {
        $myplanquemanager = new PlanqueManager();

        $newplanque = new Planque();
        $newplanque->setNomcode($_POST['nom-code']);
        $newplanque->setAdresse($_POST['adresse']);
        $newplanque->setPays((int) $_POST['pays']);
        $newplanque->setCodetype((int) $_POST['type']);

        $myplanquemanager->add($newplanque);
        header ('location: ./../../loading-page.php');
    }
    else {
        header ('location: ./../../administration.php');
    }
    