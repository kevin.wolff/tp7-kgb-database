<!-- EXE FORMULAIRE TYPE MISSION -->

<?php 
    require '../../manager/type-mission-manager.php';

    if (!empty($_POST['type'])) {
        $mytypemissionmanager = new TypeMissionManager();
    
        $newtype = new Typemission();
        $newtype->setTypemission($_POST['type']);
    
        $mytypemissionmanager->add($newtype);
        header ('location: ./../../loading-page.php');
    }
    else {
        header ('location: ./../../administration.php');
    }