<!-- DB CONNEXION -->
<?php
try
{
    $bdd = new PDO('');
}
catch (Exception $e)
{
    die('Erreur : ' . $e->getMessage());
}

session_start();

require './autoloader.php';

if(!empty($_GET['result'])) {
    if ($_GET['result'] == 'false') {
        echo "<script>confirm(\"Impossible de supprimer cet item.\")</script>";
    }
    else {
        echo "<script>confirm(\"Tout c'est bien passé\")</script>";
    }
}

// MISSION CARD MANAGER //

$mymissionmanager = new MissionManager();
$myallmission = $mymissionmanager->getAll();

$myagentmanager = new AgentManager();
$myallagent= $myagentmanager->getAll();

$myciblemanager = new CibleManager();
$myallcible = $myciblemanager->getAll();

$mycontactmanager = new ContactManager();
$myallcontact = $mycontactmanager->getAll();

$myplanquemanager = new PlanqueManager();
$myallplanque = $myplanquemanager->getAll();

// TOOL CARD MANAGER //

$mytypemanager = new TypeMissionManager();
$myalltype = $mytypemanager->getAll();

$mystatutmanager = new StatutMissionManager();
$myallstatut = $mystatutmanager->getAll();

$mypaysmanager = new PaysManager();
$myallpays = $mypaysmanager->getAll();

$myspecialitemanager = new SpecialiteManager();
$myallspecialite = $myspecialitemanager->getAll();

$mytypeplanque = new TypePlanqueManager();
$myalltypeplanque = $mytypeplanque->getAll();

?>

<!-- HTML -->
<!DOCTYPE html>
<html lang='en'>
<head>
    <meta charset='UTF-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <title>Projet-kgb</title>
    <!-- CUSTOM STYLESHEET -->
    <link rel='stylesheet' href='assets/dist/style.css'>
    <!-- CUSTOM FONT -->
    <link href="https://fonts.googleapis.com/css2?family=Graduate&family=Source+Code+Pro&display=swap" rel="stylesheet">
</head>
<body>

    <!-- NAVBAR -->
    <?php include ("./assets/php/navbar.php");?>

    <!-- HOME MSG -->
    <?php 
    // MSG IF NOT LOGED
    if(!isset($_SESSION['code_admin']))
    {
    ?>
        <div class="page-message">
            <h2>_Bienvenue visiteur</h2>
        </div>
        <div class="box-anim no-loging"></div>
    <?php
    }
    // MSG IF LOGED
    else 
    {
        ?>
        <div class="page-message">
            <h2>_Bienvenue <?= $_SESSION['prenom_admin'] . ' ' . $_SESSION['nom_admin']; ?></h2>
        </div>
        <div class="box-anim"></div>
    <?php
    }
    ?>

    <!-- WRAPPER-->
    <div class="cards-wrapper">

        <!-- DIV WITH ALL MISSION CARDS -->
        <?php include ("./assets/php/index-mission-cards.php"); ?>

        <!-- DIV WITH ALL TOOL CARDS -->
        <?php include ("./assets/php/index-tool-cards.php"); ?>
    
    </div>

    <!-- CUSTOME JS SCRIPT -->
    <script src='assets/js/index-script.js'></script>
    <script src='assets/js/navbar-toggle.js'></script>
</body>
</html>
